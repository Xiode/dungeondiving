﻿    using NotifySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterHandler {

    public bool InEncounter = false;
    public Encounter CurrentEncounter;

    Player player;
    UICombatContext UICC;

    public Combatant[] cFriendlies = new Combatant[5];
    public Combatant[] cEnemies = new Combatant[5];
    
    bool EnemyTurn = false; // is it the enemy's turn?

    private int currentTurn; // i.e. current "position" in either the Enemy or Friendlies list.

    public int CurrentTurn
    {
        get
        {
            return currentTurn;
        }
        set
        {
            try { UICC.SetTurnMarker(CurrentCombatant.Avatar); } catch { }
            currentTurn = value;
        }
    }

    public Combatant CurrentCombatant
    {
        get
        {
            return EnemyTurn ? cEnemies[CurrentTurn] : cFriendlies[CurrentTurn];
        }
    }

    public EncounterHandler(Player pl, UICombatContext cc)
    {
        player = pl;
        UICC = cc;
    }

    public void StartEncounter(Encounter e)
    {
        CurrentEncounter = e;
        
        AddCombatant(false, 0, player.PlayerCStats);

        for (int i=0; i<e.enemies.Length; i++)
        {
            Combatant c = AddCombatant(true, i, e.enemies[i]);
            
            c.Position = i;
        }

        EnemyTurn = false;
        CurrentTurn = 0; // start on player's turn
        
    }

    Combatant AddCombatant(bool Enemy, int Pos, CombatantStats cs)
    {
        Combatant c = new Combatant(cs);

        if (Enemy)
        {
            cEnemies[Pos] = c;
        }
        else
        {
            cFriendlies[Pos] = c;
        }

        //c.Avatar = UICC.AddCombatant(c); // give this combatant and avatar

        return c;
    }
    
    public void DoAction(CombatantAction cAction, Combatant Instigator, Combatant Target)
    {
        UICC.PushScene(cAction.Frame);

        if(cAction.GetType() == typeof(CombatantAttack))
        {
            //Debug.Log(Instigator.Name + " used " + cAction.Name + " on " + t.Name + "!");
            UICC.PushTickerText(Instigator.Name + " used " + cAction.Name + " on " + Target.Name + ", dealing " + ((CombatantAttack)cAction).Damage + " damage!");

            Target.Health -= ((CombatantAttack)cAction).Damage; // TODO: apply defense, etc

            Instigator.Avatar.UpdateUI();
            Target.Avatar.UpdateUI();
        }
    }

    public void NextTurn()
    {
        CurrentTurn++;

        if (EnemyTurn)
        {
            if (cEnemies[CurrentTurn] == null)
            {
                EnemyTurn = false; // If there is no next enemy, flip.
                CurrentTurn = 0;
            }
        }
        else
        {
            if (cFriendlies[CurrentTurn] == null)
            {
                EnemyTurn = true;
                CurrentTurn = 0;
            }
        }
        
        if (CurrentCombatant.Neutralized)
        {
            Debug.Log("Skipping.");
            NextTurn();
            return;
        }

        Debug.Log("next turn! " + CurrentTurn);
    }
}
/*
    public bool InEncounter = false;
    
    public delegate void EncounterDelegate();

    public EncounterDelegate OnStart;
    public EncounterDelegate OnEnd;

    public Combatant[] Friendlies = new Combatant[5];
    public Combatant[] Enemies = new Combatant[5];

    bool AllEnemiesNeutralized = false;

    public Player player;
    
    
    public void Update()
    {
        AllEnemiesNeutralized = true;
        foreach (Combatant c in Enemies)
        {
            if (c != null && c.HP > 0 && c.AR < c.MaxAR)
                AllEnemiesNeutralized = false;
        }

        if (InEncounter && AllEnemiesNeutralized)
            EndEncounter();
    }

    public void StartEncounter(Encounter e, Player pl)
    {
        player = pl;

        for (int i = 0; i < e.enemies; i++)
        {
            Enemies[i] = new Combatant(e.enemyType);
            
            Enemies[i].OnDamaged += (int v, int hp) =>
            {
                //Debug.Log("Enemy" + i + " (" + Enemies[i].Name + ") took " + v + " damage! At " + hp + "HP!");
                player.Notify("Enemy" + i + "_Damaged", v);
                player.Notify("Enemy" + i + "_HP", hp);
            };

            Enemies[i].OnArouse += (int v, int ar) =>
            {
                //Debug.Log("Enemy" + i + " (" + Enemies[i].Name + ") aroused " + v + " points! At " + ar + "AR!");
                player.Notify("Enemy" + i + "_Aroused", v);
                player.Notify("Enemy" + i + "_AR", ar);
            };

            player.Notify("Enemy" + i + "_HP", Enemies[i].HP);
        }
        
        Friendlies[0] = new Combatant(player);
        
        // announce HP at start

        InEncounter = true;

        if (OnStart != null)
            OnStart();
    }
    
    public void EndEncounter()
    {
        if (InEncounter)
        {
            if (OnEnd != null)
            OnEnd();

            player.OnNotify("EndEncounter", null);

            Debug.Log("End encounter.");

            InEncounter = false;
        }
    }
    
    public void DoTurn(Combatant c, CombatAction ca)
    {
        if (ca == null)
            ca = c.Action(this);


        string message = c.Name + " ";

        switch (ca.Action)
        {
            case CombatAction.action.Attack:
                GetCombatant(ca.Target).Damage(c.Dmg);
                message += "attacked " + GetCombatant(ca.Target).Name + ", dealing " + c.Dmg + " damage!";
                break;
            case CombatAction.action.Defend:
                message += "defended.";
                break;
            case CombatAction.action.Lewd:
                GetCombatant(ca.Target).Arouse(c.ArDmg);
                c.Arouse(Mathf.Max(c.ArDmg - 1, 1));
                message += "sex'd " + GetCombatant(ca.Target).Name + ", arousing them " + c.ArDmg + " points, and themselves " + Mathf.Max(c.ArDmg-1,1) + " points!";
                break;
        }


        player.OnNotify("UI_AddEncounterText", message);
    }

    Combatant GetCombatant(string target)
    {
        // I mean, whatever, I guess.
        switch (target.ToLower())
        {
            case "player":
                return Friendlies[0];
            case "partner":
                return Friendlies[1];
            case "enemy0":
                return Enemies[0];
            case "enemy1":
                return Enemies[1];
            case "enemy2":
                return Enemies[2];
            default:
                return Enemies[0];
        }
    }

}
*/