﻿using NotifySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Mover : Member
{
    public GameRules gameRules;

    private Rigidbody rb;

    // Checks if we're in the middle of a movement or spin
    public bool CanMove
    {
        get { return (!Moving && !Turning); }
    }

    private bool Moving = false;
    private bool Turning = false;

    public float CheckMoveDist = 3f; // how far ahead of us do we check when seeing if this mover can go somewhere without hitting a wall?

    public float TurnSpeed = 0.5f; // linear turn speed
    public float TurnWindUpTime = 0.5f; // how long should it take go from stationary to TurnSpeed?
    private float TurnWindUp = 0f; // tracks initial spin wind up. this is reset to 0 at the start of a turn.
    
    int RotFace; // Rotation Facing. 0-3. this*90 determines which direction the mover faces

    public MoveCommand LastMove; // Holds the most recent move.

    public delegate void _OnMove();

    public _OnMove OnMove;
    public _OnMove OnMoveComplete;
    public _OnMove OnMoveFail;

    // Unused
    public override void OnNotify(string msg, object obj)
    { }

    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody>();
    }


    // Update is called once per frame
    void Update () {
        if (Turning && TurnWindUp < 1) //TurnWindUp is reset to 0 whenever we start a spin
            TurnWindUp += Time.deltaTime * TurnWindUpTime;
    }

    // Called at a regular interval, in sync with the physics engine.
    void FixedUpdate()
    {
        if (Moving && CheckMoveComplete(LastMove)) // If we're not where we're supposed to be in accordance with the LastMove, check until we are
        {
            if (OnMoveComplete != null && LastMove.Tracked) OnMoveComplete(); // once CheckMoveComplete returns true, fire this
            Moving = false; // show that we're done moving
        }

        if (Turning && !Moving) // We shouldn't ever be turning and moving
            CheckSpinComplete();
        
    }
    

    bool CheckCanMove(MoveCommand move)
    {
        LayerMask lm = LayerMask.GetMask("World");

        Debug.DrawLine(transform.position + transform.up, transform.position + transform.up + ((move.GoalPos - move.StartPos).normalized * 3f), Color.red, 5f);

        bool r = !Physics.Linecast(transform.position + transform.up, transform.position + transform.up + ((move.GoalPos - move.StartPos).normalized * CheckMoveDist), lm);

        if (!r && OnMoveFail != null && move.Tracked)
            OnMoveFail();

        return r;
    }
    
    bool CheckMoveComplete(MoveCommand move)
    {
        Vector3 movement = Vector3.ClampMagnitude(move.GoalPos - transform.position, gameRules.MovementSpeed * Time.fixedDeltaTime);
        
        if (!move.Started)
        {
            if (CheckCanMove(move))
            {
                move.Started = true;
                if (OnMove != null && move.Tracked) OnMove();
            }
            else
            {
                if (OnMoveFail != null && move.Tracked) OnMoveFail();
                CancelMove();
                return true;
            }
        }

        rb.MovePosition(transform.position + movement);
        
        if (Moving && movement.magnitude < 0.1f)
        {
            rb.MovePosition(move.GoalPos);
            return true;
        }

        return false;
    }

    bool CheckSpinComplete()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.AngleAxis(RotFace*90f, Vector3.up), TurnSpeed * TurnWindUp);

        if (Turning && TurnWindUp >= 1 && (int)(transform.rotation.eulerAngles.y + .1) % 90 == 0)
        {
            transform.rotation = Quaternion.AngleAxis(RotFace*90f, Vector3.up);
            Turning = false;
            return true;
        }
        return false;
    }

    public void Teleport(Vector3 location)
    {
        transform.position = MoveTo(location).GoalPos;
    }

    public MoveCommand Move(Vector3Int move, bool useGrid = true, bool trackMove = true)
    {
        MoveCommand mc = new MoveCommand();

        mc.StartPos = transform.position;
        if (useGrid)
            mc.GoalPos = (Quantize(transform.position) + (move * gameRules.StepSize));
        else
            mc.GoalPos = (transform.position) + (Vector3)(move * gameRules.StepSize);

        mc.Tracked = trackMove;

        return Move(mc);
    }

    public MoveCommand Move(MoveCommand mc)
    {
        Moving = true;
        LastMove = mc;
        return mc;
    }

    // Move to position
    public MoveCommand MoveTo(Vector3 goal, bool useGrid = true, bool trackMove = true)
    {
        MoveCommand mc = new MoveCommand();

        mc.StartPos = transform.position;

        //mc.GoalPos = (useGrid ? (Quantize(goal)) : (goal);
        if (useGrid)
            mc.GoalPos = Quantize(goal);
        else
            mc.GoalPos = goal;
        
        mc.Tracked = trackMove;

        return Move(mc);
    }

    Vector3Int Quantize(Vector3 v)
    {
        int stepsize = gameRules.StepSize;
        int offset = stepsize / 2;

        return new Vector3Int(
            Mathf.FloorToInt((v.x + offset) / stepsize) * stepsize,
            Mathf.FloorToInt((v.y + offset) / stepsize) * stepsize,
            Mathf.FloorToInt((v.z + offset) / stepsize) * stepsize);
    }


    public void CancelMove()
    {
        MoveTo(LastMove.StartPos, false, false);
    }

    public void Spin(int spin)
    {
        Turning = true;
        RotFace -= spin;
        TurnWindUp = 0f;

        if (RotFace < 0)
            RotFace += 4;

        if (RotFace >= 4)
            RotFace -= 4;

    }

    public void SpinTo(float angle)
    {
        RotFace = (int)((angle + 45) / 90);
    }
}

public class MoveCommand
{
    public Vector3Int Command;
    public Vector3 StartPos;
    public Vector3 GoalPos;
    public bool Tracked; // should this command be tracked, and trigger OnMoveComplete/OnMoveFail?

    public bool Started;

    public MoveCommand()
    {
        Started = false;
    }
}
