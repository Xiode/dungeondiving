﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NotifySystem;

public class Door : Interactable {

    /// <summary>
    /// The other door that this door leads to.
    /// </summary>
    public Door LinkedDoor;
    /// <summary>
    /// Object reference for where the player should spawn after traveling to this door.
    /// </summary>
    public Transform Spawnpoint;

    // Use this for initialization
    void Start()
    {
        AddObserver(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>());
    }

    public override void Exec()
    {
        Notify("Player_UseDoor", LinkedDoor.Spawnpoint.transform);
    }

    public override void OnNotify(string msg, object obj)
    {}

}
