﻿using NotifySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FanTrap : Member {

    public TrapTrigger tt;

    // Use this for initialization
    void Start () {
        AddObserver(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>());

        tt.OnEnter += (Collider c) =>
        {
            if (c.tag == "Player")
            {
                Notify("Player_Move", (Vector3Int)(-transform.right.normalized));
            }
        };
	}

    public override void OnNotify(string msg, object obj)
    {
    }
}
