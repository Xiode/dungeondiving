﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Generic trigger controller for traps

public class TrapTrigger : MonoBehaviour {
    
    public delegate void onCEvent(Collider c);
    public onCEvent OnEnter;
    public onCEvent OnExit; 
    
    void OnTriggerEnter(Collider c)
    {
        if (OnEnter != null)
            OnEnter(c);
    }

    void OnTriggerExit(Collider c)
    {
        if (OnExit != null)
            OnExit(c);
    }
}
