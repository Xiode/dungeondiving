﻿using NotifySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PullTrap : Member {
    
    public TrapTrigger tt;
    public Transform playerHold;
    public StatusScene scene; // the scene to show when caught in this trap
    // TODO: Add animations, etc

    public int Damage = 1;
    public int Arousal = 5;

    // where to put the player once the trap is done
    private Vector3 playerPos;

    public int KeymashiesRequired = 20;
    int Keymashies;

    bool trapInUse = false;

    public override void OnNotify(string msg, object obj)
    {
    }

    // Use this for initialization
    void Start () {
        AddObserver(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>());

        tt.OnEnter += (Collider c) =>
        {
            if (c.tag == "Player" && !trapInUse)
            {
                StartTrap(c.transform.position);
            }
        };

        tt.OnExit += (Collider c) =>
        {
            if (c.tag == "Player")
            {
                ResetTrap();
            }
        };
    }

    void Update()
    {
        CheckTrap();
    }

    void CheckTrap()
    {
        if (trapInUse && Input.GetKeyDown(KeyCode.Space))
        {
            if (Keymashies < KeymashiesRequired)
            {
                Keymashies++;
                Notify("Player_Arouse", 1);
            }
            else
            {
                Notify("Player_Damage", Damage);
                EndTrap();
            }
        }
    }

    /// <summary>
    /// Pull the player into the trap.
    /// </summary>
    void StartTrap(Vector3 ppos)
    {
        trapInUse = true;
        playerPos = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().mover.LastMove.GoalPos;
        Notify("Player_EnterTrap", playerHold.position);
        Notify("Player_Trapped", true);
        if (scene)
            Notify("Player_SetStatus", scene);
    }

    /// <summary>
    /// Release the player from the trap.
    /// </summary>
    void EndTrap()
    {
        Notify("Player_Trapped", false);
        Notify("Player_ExitTrap", playerPos);
        trapInUse = false;
    }

    /// <summary>
    /// Reset the trap for the next use.
    /// </summary>
    void ResetTrap()
    {
        Keymashies = 0;
    }
}
