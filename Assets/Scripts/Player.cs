﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NotifySystem;
using System;

public class Player : Member
{
    public GameRules gameRules;

    public CombatantStats PlayerCStats;

    [HideInInspector]
    public Mover mover;

    public int Health = 20;
    public int MaxHealth = 20;

    public int Arousal = 0;
    public int MaxArousal = 100;

    public Image UIStatus;

    public StatusScene Normal;
    public StatusScene CombatIdle;

    public UIIngameContext IngameContext;
    public UIExploreContext ExploreContext;
    public UICombatContext CombatContext;

    bool InTrap = false; // get rid of this

    //public EncounterHandler encounterHandler;

    public CombatTest fugg;
    
    public override void OnNotify(string msg, object obj)
    {
        switch (msg)
        {
            case "Player_Move":
                mover.Move((Vector3Int)obj);
                break;
            case "Player_CancelMove":
                mover.CancelMove();
                break;
            case "Player_UseDoor":
                UseDoor((Transform)obj);
                break;
            case "Player_EnterTrap":
                mover.MoveTo((Vector3)obj, false, false);
                break;
            case "Player_ExitTrap":
                mover.MoveTo((Vector3)obj, true, true);
                SetStatus(Normal);
                break;
            case "Player_Trapped":
                InTrap = (bool)obj;
                break;
            case "Player_Damage":
                Damage((int)obj);
                break;
            case "Player_Arouse":
                Arouse((int)obj);
                break;
            case "Player_SetStatus":
                SetStatus((StatusScene)obj);
                break;
            case "StartEncounter":
                StartEncounter((Encounter)obj);
                break;
            case "EndEncounter":
                EndEncounter();
                break;

                /*
            case "UIPressed_Attack":
                //encounterHandler.DoTurn(encounterHandler.Friendlies[0], new CombatAction(CombatAction.action.Attack, "enemy0"));
                break;
            case "UIPressed_Defend":
                // waste a turn!
                break;
            case "UIPressed_Lewd":
                //encounterHandler.DoTurn(encounterHandler.Friendlies[0], new CombatAction(CombatAction.action.Lewd, "enemy0")); // slut it up!
                break;
            case "UIPressed_Flee":
                EndEncounter(); // run away!
                break;
            case "UI_AddEncounterText":
                //CombatContext.AddEncounterText(obj.ToString());
                break;*/
        }
    }

    void Awake()
    {
        GameDefaults.Init(); // Set up game defaults
        GameDefaults.defaultGameRules = gameRules;
    }

    // Use this for initialization
    void Start()
    {
        //encounterHandler = new EncounterHandler(this, CombatContext);
        fugg = new CombatTest();
        
        Meta.onTick += TickUpdate;
        Meta.GameplayState = Meta._gameplayState.Explore;

        SetStatus(Normal);
        SetUIContext(Meta.GameplayState);

        mover = GetComponent<Mover>();
        mover.OnMoveComplete += () =>
        {
            Meta.Tick(); // Tick the whole gamestate forward every time we complete a move
        };

        /*
        encounterHandler.OnEnd += () =>
        {
            Meta.GameplayState = Meta._gameplayState.Explore;
        };*/

        //CombatContext.EncounterMenu.AddObserver(this);

        GInput.ResetBinds();

        GInput.OnDown += (GInput.GVerb v) =>
        {
            if (mover.CanMove && !InTrap && Meta.GameplayState == Meta._gameplayState.Explore) // probably refactor all this shit
            {
                if (v == GInput.GVerb.wForward)
                {
                    mover.Move(transform.forward);
                }
                if (v == GInput.GVerb.wBackward)
                {
                    mover.Move(-transform.forward);
                }

                if (v == GInput.GVerb.wLeft)
                {
                    mover.Spin(1);
                }
                if (v == GInput.GVerb.wRight)
                {
                    mover.Spin(-1);
                }

                if (v == GInput.GVerb.wUse)
                {
                    Use();
                }

            }

            if (Meta.GameplayState == Meta._gameplayState.Combat)
            {
                if (v == GInput.GVerb.wLeft) ;
                //CombatContext.MoveSelect(-1);

                if (v == GInput.GVerb.wRight);
                //CombatContext.MoveSelect(1);

                if (v == GInput.GVerb.menuConfirm)
                {
                    int f = fugg.Seek();

                    print(fugg.fighters[f].Name + "'s turn! (" + f + ")");
                    CombatContext.SetTurnMarker(f);

                    //encounterHandler.DoAction(PlayerCStats.Attacks[0], encounterHandler.CurrentCombatant, encounterHandler.cEnemies[0]);
                    //encounterHandler.NextTurn();
                }
            }
        };

        GInput.OnDown += OnInputDown;
    }

    void OnInputDown(GInput.GVerb v)
    {

    }

    void TickUpdate()
    {
    }

    // Update is called once per frame
    void Update ()
    {
        GInput.Update(); // this should ONLY happen here
        /*
        if (mover.CanMove && !InTrap && Meta.GameplayState == Meta._gameplayState.Explore) // probably refactor all this shit
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                mover.Move(transform.forward);
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                mover.Move(-transform.forward);
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                mover.Spin(1);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                mover.Spin(-1);
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                Use();
            }
        }
        if (Meta.GameplayState == Meta._gameplayState.Combat)
        {
            if (Input.GetKeyDown(KeyCode.A));
                //CombatContext.MoveSelect(-1);

                if (Input.GetKeyDown(KeyCode.D)) ;
                //CombatContext.MoveSelect(1);
        }
        */

        //encounterHandler.Update();
    }


    // 'use' whatever's in front of us
    void Use()
    {
        LayerMask lm = LayerMask.GetMask("Default");

        RaycastHit[] hits = Physics.RaycastAll(Camera.main.transform.position, transform.forward, gameRules.StepSize*.7f, lm);

        foreach (RaycastHit hit in hits)
        {
            Interactable i = hit.transform.gameObject.GetComponent<Interactable>();
            try
            {
                i.Exec();
            } catch (Exception ex) { ex.ToString(); }
        }
    }

    // teleport the player when they use a door
    void UseDoor(Transform doorTgt)
    {
        mover.Teleport(doorTgt.position);
        mover.SpinTo(doorTgt.rotation.eulerAngles.y+180);
    }

    public void Damage(int value)
    {
        print(Health + " - " + value);
        Health = Mathf.Clamp(Health - value, 0, MaxHealth);
        Notify("Player_HP", Health);
    }

    public void Arouse(int value)
    {
        print(Arousal + " + " + value);
        Arousal = Mathf.Clamp(Arousal + value, 0, MaxArousal);
        Notify("Player_AR", Arousal);
    }

    void SetStatus(StatusScene s)
    {
        UIStatus.sprite = s.sprite;
        UIStatus.color = new Color(1,1,1,s.Opacity);
        print(s.Opacity);
    }

    void StartEncounter(Encounter e)
    {
        Meta.GameplayState = Meta._gameplayState.Combat;
        SetUIContext(Meta._gameplayState.Combat);
        SetStatus(CombatIdle);
        
        fugg.StartEncounter(this, PlayerCStats, null, e);

        CombatContext.StartEncounter(fugg);

        //encounterHandler.StartEncounter(e);
    }

    void EndEncounter()
    {
        print("VICTORY");

        Meta.GameplayState = Meta._gameplayState.Explore;
        SetUIContext(Meta._gameplayState.Explore);

        SetStatus(Normal);
    }
    
    void SetUIContext(Meta._gameplayState state)
    {
        switch (state)
        {
            case Meta._gameplayState.Explore:
                DisableUIContexts();

                IngameContext.gameObject.SetActive(true);
                ExploreContext.gameObject.SetActive(true);

                break;
            case Meta._gameplayState.Combat:
                DisableUIContexts();

                //IngameContext.gameObject.SetActive(true);
                CombatContext.gameObject.SetActive(true);

                break;
        }
    }

    //disable all the UI contexts. wipe the slate clean.
    void DisableUIContexts()
    {
        IngameContext.gameObject.SetActive(false);
        ExploreContext.gameObject.SetActive(false);
        CombatContext.gameObject.SetActive(false);
    }

}
