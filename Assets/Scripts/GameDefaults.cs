﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this all gets initialized and set up by the Player script

public static class GameDefaults
{

    public static GameRules defaultGameRules;

    public static void Init()
    {
        Application.targetFrameRate = 300;
        QualitySettings.vSyncCount = 0;
    }

}