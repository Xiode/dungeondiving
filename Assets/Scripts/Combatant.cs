﻿using NotifySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combatant {

    public string Name = "Fighter";

    public int Health = 5;
    public int MaxHealth = 5;

    public int Arousal = 0;
    public int MaxArousal = 5;

    public bool isEnemy = true;
    public int Position = 0;

    public bool Neutralized = false;

    public UICombatant Avatar;

    public Combatant(CombatantStats c)
    {
        Init(c);
    }

    public Combatant(Player pl)
    {
        Init(pl.PlayerCStats);

        Health = pl.Health;
        MaxHealth = pl.MaxHealth;

        Arousal = pl.Arousal;
        MaxArousal = pl.MaxArousal;
    }

    void Init(CombatantStats c)
    {
        Name = c.Name;

        isEnemy = c.Enemy;

        Health = c.HP;
        MaxHealth = c.MaxHP;

        Arousal = c.AR;
        MaxArousal = c.MaxAR;
    }
}
    /*
    public string Name = "Fighter";

    public Sprite Status;

    public int HP; // health
    public int MaxHP;
    public int AR; // arousal
    public int MaxAR;

    public int Dmg = 1;
    public int ArDmg = 1;

    public delegate void EnemyDelegate();
    public delegate void EnemyDelegateStat(int value, int final);

    public EnemyDelegateStat OnDamaged;
    public EnemyDelegateStat OnArouse;
    public EnemyDelegate OnNeutralized;
    public EnemyDelegate OnDie;

    public bool Neutralized
    {
        get
        {
            return HP<=0 || AR>=MaxAR;
        }
    }

    public Combatant(EnemyType e)
    {
        Name = e.Name;
        HP = e.HP;
        MaxHP = e.MaxHP;
        AR = e.AR;
        MaxAR = e.MaxAR;
        Status = e.IdleSprite;
    }

    public Combatant(Player pl)
    {
        Name = "Player";
        HP = pl.Health;
        MaxHP = pl.MaxHealth;
        AR = pl.Arousal;
        MaxAR = pl.MaxArousal;
        Status = pl.UIStatus.sprite;
    }

    public Combatant(string name, int hp, int maxhp, int ar, int maxar)
    {
        Name = name;
        HP = hp;
        MaxHP = maxhp;
        AR = ar;
        MaxAR = maxar;
    }
    
    public void Damage(int value)
    {
        HP = Mathf.Clamp(HP - value, 0, MaxHP);

        if (OnDamaged != null)
            OnDamaged(value, HP);
    }

    public void Arouse(int value)
    {
        AR = Mathf.Clamp(AR + value, 0, MaxAR);

        if (OnArouse != null)
            OnArouse(value, AR);
    }
    
    public CombatAction Action(EncounterHandler ehand)
    {
        // TEST AI
        if (HP < MaxHP / 4)
            return new CombatAction(CombatAction.action.Defend, "self");

        if (AR > MaxAR / 2)
        {
            if (ehand.player.Health < ehand.player.MaxHealth*.75 || ehand.player.Arousal > ehand.player.MaxArousal*.3 || HP > MaxHP/2)
                return new CombatAction(CombatAction.action.Lewd, "player");

            return new CombatAction(CombatAction.action.Lewd, "self");
        }

        return new CombatAction(CombatAction.action.Attack, "player");
    }
}

public class CombatAction
{
    public enum action {
        None, // do nothing
        Attack, // attack
        Defend, // defend
        Stunned, // be stunned
        Lewd
    };

    public action Action;
    public string Target;

    public CombatAction(action A, string targ)
    {
        Action = A;
        Target = targ.ToLower();
    }
}
*/