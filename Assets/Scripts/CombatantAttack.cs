﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Action - Attack")]
public class CombatantAttack : CombatantAction
{
    public int Damage;
}
