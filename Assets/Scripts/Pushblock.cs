﻿using NotifySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Pushblock : Member {

    Mover mover;

	// Use this for initialization
	void Start () {
        AddObserver(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>());

        mover = GetComponent<Mover>();

        mover.OnMoveFail += () =>
        {
            Notify("Player_CancelMove");
        };

    }

    public override void OnNotify(string msg, object obj)
    {
    }

    void OnTriggerEnter(Collider c)
    {
        Mover m = c.gameObject.GetComponent<Mover>();
        //Mover rm = c.gameObject.GetComponent<RBMover>();
        if (m != null)
        {
            mover.Move(m.LastMove);
        }
    }
}
