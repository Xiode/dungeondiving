﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Action - Defense")]
public class CombatantDefense : CombatantAction
{
    public int Protection;
}
