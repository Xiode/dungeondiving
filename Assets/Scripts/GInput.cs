﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Game Input

public static class GInput {

    public enum GVerb
    {
        None,

        // World verbs
        wForward,
        wBackward,
        wLeft,
        wRight,
        wUse,
        
        // UI and Menu verbs
        menuConfirm,
        menuCancel,
        menuSelect1,
        menuSelect2
    }

    public static Dictionary<KeyCode, GVerb> Binds = new Dictionary<KeyCode, GVerb>();
    
    public delegate void OnGKeyInput(KeyCode key);
    public static OnGKeyInput OnKeyDown;
    public static OnGKeyInput OnKeyUp;
    public static OnGKeyInput OnKeyHold;

    public delegate void OnGVerbInput(GVerb verb);
    public static OnGVerbInput OnDown;
    public static OnGVerbInput OnUp;
    public static OnGVerbInput OnHold;

    public static void Update() // Checks for input here, triggered inside Player update
    {
        foreach (KeyValuePair<KeyCode, GVerb> kvp in Binds)
        {
            if (Input.GetKeyDown(kvp.Key))
            {
                if (OnKeyDown != null) OnKeyDown(kvp.Key);
                if (OnDown != null) OnDown(kvp.Value);
            }
            if (Input.GetKeyUp(kvp.Key))
            {
                if (OnKeyUp != null) OnKeyUp(kvp.Key);
                if (OnUp != null) OnUp(kvp.Value);
            }
            if (Input.GetKey(kvp.Key))
            {
                if (OnKeyHold != null) OnKeyHold(kvp.Key);
                if (OnHold != null) OnHold(kvp.Value);
            }
        }
    }

    public static void Bind(KeyCode key, GVerb verb)
    {
        Binds.Add(key, verb);

    }

    public static void ClearKey(KeyCode key)
    {
        Binds[key] = GVerb.None;
    }

    public static void ClearVerb(GVerb verb)
    {
        foreach (KeyValuePair<KeyCode, GVerb> kvp in Binds)
        {
            if (kvp.Value == verb)
            {
                Binds.Remove(kvp.Key);
            }
        }
    }

    public static void ResetBinds()
    {
        Bind(KeyCode.W, GVerb.wForward);
        Bind(KeyCode.A, GVerb.wLeft);
        Bind(KeyCode.S, GVerb.wBackward);
        Bind(KeyCode.D, GVerb.wRight);
        Bind(KeyCode.E, GVerb.wUse);

        Bind(KeyCode.Space, GVerb.menuConfirm);
        Bind(KeyCode.Escape, GVerb.menuCancel);
    }
}
