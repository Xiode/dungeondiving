﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Encounter")]
public class Encounter : ScriptableObject {

    public CombatantStats[] enemies;
    // todo: add environment type info
}
