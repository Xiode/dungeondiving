﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Status Scene")]
public class StatusScene : ScriptableObject {

    public Sprite sprite;
    public float Opacity = 1f;
    // TODO: Duration? text? sounds? etc?
    
}
