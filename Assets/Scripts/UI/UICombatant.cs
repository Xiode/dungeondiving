﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UICombatant : MonoBehaviour {

    public UICombatantMeter HPMeter;
    public UICombatantMeter ARMeter;

    public string Name;
    public Image Status;
    public bool Enemy; // which side of the field is this character on?

    public int Index;

    public CTCombatant combatant;

    public void Init(CTCombatant c, int id)
    {
        Status = GetComponent<Image>();

        combatant = c;
        Index = id;

        Status.sprite = c.Spr;

        if (id < 5)
            Enemy = false;
        else
            Enemy = true;

        HPMeter.Value = 0;
        ARMeter.Value = 0;
        
        UpdateUI();
    }

    public void UpdateUI()
    {
        // wow fancy tweening!
        print(combatant.Name + (float)combatant.Health / (float)combatant.MaxHealth);
        print(combatant.Name + (float)combatant.Arousal / (float)combatant.MaxArousal);
        DOTween.To(() => HPMeter.Value, x => HPMeter.Value = x, (float)combatant.Health / (float)combatant.MaxHealth, .5f);
        DOTween.To(() => ARMeter.Value, x => ARMeter.Value = x, (float)combatant.Arousal / (float)combatant.MaxArousal, .5f);
    }
}
