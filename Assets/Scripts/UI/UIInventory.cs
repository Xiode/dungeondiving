﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInventory : MonoBehaviour {

    public Player pl;

    public RectTransform ItemListArea;
    public Vector2 ItemAreaSpan = new Vector2(6, 3);

    public GameObject ItemSlotPrefab;

    GameObject[] itemGrid;

	// Use this for initialization
	void Start () {
        if (pl == null)
            pl = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        itemGrid = new GameObject[Mathf.FloorToInt(ItemAreaSpan.x * ItemAreaSpan.y)];

        for (int i=0; i<ItemAreaSpan.x*ItemAreaSpan.y; i++)
        {
            itemGrid[i] = Instantiate(ItemSlotPrefab, ItemListArea);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
