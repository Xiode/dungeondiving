﻿using NotifySystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICombatContext : Member
{
    //Player player;

    public UIEncounterMenu ActionMenu;
    public UIEncounterTicker EncounterTicker;
    public UIEncounterDisplay Display;

    public RectTransform FriendliesContainer;
    public RectTransform EnemiesContainer;

    public RectTransform TurnMarker;
    public float TurnMarkerXOffset;

    public GameObject CombatantPrefab;

    //List<UICombatant> UICombatants = new List<UICombatant>();

    UICombatant[] UICombatants = new UICombatant[10];

    public override void OnNotify(string msg, object obj)
    {
    }

    public void StartEncounter(CombatTest e)
    {
        for (int i = 0; i < e.fighters.Length; i++)
        {
            if (!e.fighters[i].Inactive)
            {
                AddCombatant(e.fighters[i], i);
            }
        }
    }

    public UICombatant AddCombatant(CTCombatant c, int id)
    {
        UICombatant uic = Instantiate(CombatantPrefab, c.IsEnemy ? EnemiesContainer : FriendliesContainer).GetComponent<UICombatant>();

        uic.Init(c, id);

        uic.transform.SetSiblingIndex(c.Index);

        UICombatants[id] = uic;

        if (c.IsEnemy)
        {
            ActionMenu.AddAttackTarget(c);
        }

        return uic;
    }

    public void PushScene(Sprite s)
    {
        Display.UpdateDisplay(s);
    }

    public void PushTickerText(string t)
    {
        EncounterTicker.PushText(t);
    }

    public void SetTurnMarker(UICombatant uic)
    {
        try
        {
            TurnMarker.position = new Vector2(uic.transform.position.x + TurnMarkerXOffset, TurnMarker.position.y);
        }
        catch (Exception e)
        {
            print(e);
        }

    }

    public void SetTurnMarker(int pos)
    {
        try
        {
            TurnMarker.position = new Vector2(UICombatants[pos].transform.position.x + TurnMarkerXOffset, TurnMarker.position.y);
        }
        catch (Exception e)
        {
            print(e);
        }

    }
}