﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEncounterTicker : MonoBehaviour {

    public GameObject TextBlockPrefab;

	// Use this for initialization
	void Start ()
    {
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PushText(string text)
    {
        GameObject tb = Instantiate(TextBlockPrefab, transform);
        tb.transform.SetAsFirstSibling();
        tb.GetComponent<Text>().text = text;
    }
}
