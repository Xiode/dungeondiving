﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICombatantMeter : MonoBehaviour {

    public float MinWidth = 0;
    public float MaxWidth = 100;

    private float _value;

    public float Value
    {
        get
        {
            return _value;
        }
        set
        {
            _value = value;
            rect = GetComponent<RectTransform>();
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, MinWidth + (_value * MaxWidth));
            //rect.sizeDelta = new Vector2(MinWidth + (_value * MaxWidth), rect.sizeDelta.y);
        }
    }

    RectTransform rect;

    void Start()
    {
        rect = GetComponent<RectTransform>();
        Value = 0;
    }

}
