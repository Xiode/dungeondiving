﻿using NotifySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEncounterMenu : Member {

    public Transform AttackMenu;
    public Transform TargetList;
    public GameObject TargetListingPrefab;

    public Button UBAttack;
    public Button UBDefend;
    public Button UBLewd;
    public Button UBFlee;

	// Use this for initialization
	void Awake ()
    {
        UBAttack.onClick.AddListener(OnPressAttack);
        UBDefend.onClick.AddListener(delegate { Notify("UIPressed_Defend"   ); });
        UBLewd  .onClick.AddListener(delegate { Notify("UIPressed_Lewd"     ); });
        UBFlee  .onClick.AddListener(delegate { Notify("UIPressed_Flee"     ); });
    }

    public override void OnNotify(string msg, object obj)
    {
    }

    void OnPressAttack()
    {
        Notify("UIPressed_Attack");
        AttackMenu.gameObject.SetActive(!AttackMenu.gameObject.activeSelf);
    }

    public void AddAttackTarget(CTCombatant c)
    {
        UIAttackMenuTarget listing = Instantiate(TargetListingPrefab, TargetList).GetComponent<UIAttackMenuTarget>();

        listing.text.text = c.Name;
    }
}
