﻿using NotifySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Generic debug tool, recieves all notifications and sets associated Text to obj.ToString();

public class UITextTracker : Member {

    public string Message;
    Text text;

    public override void OnNotify(string msg, object obj)
    {
        if (msg == Message)
            text.text = obj.ToString();
    }

    // Use this for initialization
    void Start () {
        NotifySystem.NotifySystem.SuperObservers.Add(this);
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
