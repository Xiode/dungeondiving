﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

// name later: CombatStage
public class CombatTest1 : MonoBehaviour {

    public Image CombatViewL1;
    public Image CombatViewL2;

    public Transform FriendlyContainer;
    public Transform EnemyContainer;
    
    public GameObject EmptyCombatantPrefab;
    public int CombatantsPerSide = 5;

    public UICT1Combatant[] ffighters;
    public UICT1Combatant[] efighters;

    public GameObject Pointer;

    public Transform CombatLogText;
    List<Transform> CombatLogs;

    UICT1Combatant CurTurn;

    UIselectable Selected;
    UIselectable Selected2;
    UIselectable CancelTarget;

    public AudioSource UIAudio;

    public AudioClip Snd_PointerMove;
    public AudioClip Snd_Confirm;

    public GameObject CombatLogTextPrefab;

    public GameObject ActionMenuPrefab;
    
    public GameObject ButtonPrefabAttack;
    public GameObject ButtonPrefabLewd;
    public GameObject ButtonPrefabUse;

    public GameObject LabelPrefab;

    public Sprite DebugBunSymbol;
    public Sprite DebugCatSymbol;
    public Sprite DebugMeanCatSymbol;

    public Sprite DebugBunAttack;
    public Sprite DebugCatHit;


    // todo: make this StartEncounter, triggered when an encounter launches
    void Start ()
    {
        CombatLogs = new List<Transform>();

        GameObject g = Instantiate(CombatLogTextPrefab, CombatLogText);
        g.GetComponent<Text>().text = "Battle start!";
        CombatLogs.Add(g.transform);

        // init arrays
        ffighters = new UICT1Combatant[CombatantsPerSide];
        efighters = new UICT1Combatant[CombatantsPerSide];

        // spawn a bunch of empty disabled dudes in each array
        PopulateTeam(ffighters, FriendlyContainer);
        PopulateTeam(efighters, EnemyContainer);

        // these make it so you can loop through all the guys on the screen in either direction
        ffighters[4].GetComponent<UIselectable>().NextRight = efighters[0].GetComponent<UIselectable>();
        efighters[0].GetComponent<UIselectable>().NextLeft = ffighters[4].GetComponent<UIselectable>();
        ffighters[0].GetComponent<UIselectable>().NextLeft = efighters[4].GetComponent<UIselectable>();
        efighters[4].GetComponent<UIselectable>().NextRight = ffighters[0].GetComponent<UIselectable>();
        
        // Start with the player selected and on his turn
        Select(ffighters[4].GetComponent<UIselectable>());
        CurTurn = ffighters[4];

        // 2: populate each fighter. all this is me doing it manually for testing right now, 
        // I want to have it either auto-populated or populated in the unity editor later

        // player
        ffighters[4].gameObject.SetActive(true);
        ffighters[4].CharacterName = "Arthur";
        ffighters[4].spr = DebugBunSymbol;
        ffighters[4].ApplicableActions.Add(new CombatAction("Lewd"));
        ffighters[4].ApplicableActions.Add(new CombatAction("Use..."));
        ffighters[4].AttackScene = DebugBunAttack;

        // player's partner
        ffighters[3].gameObject.SetActive(true);
        ffighters[3].CharacterName = "Kat";
        ffighters[3].spr = DebugCatSymbol;
        ffighters[3].ApplicableActions.Add(new CombatAction("Lewd"));
        ffighters[3].ApplicableActions.Add(new CombatAction("Use..."));

        // an enemy
        efighters[0].gameObject.SetActive(true);
        efighters[0].CharacterName = "Bloodskritch Goon";
        efighters[0].spr = DebugMeanCatSymbol;
        efighters[0].ApplicableActions.Add(new CombatAction("Attack"));
        efighters[0].ApplicableActions.Add(new CombatAction("Lewd"));
        efighters[0].ApplicableActions.Add(new CombatAction("Use..."));
        efighters[0].HurtScene = DebugCatHit;

    }
    
    // spawn a bunch of empty combatants in each team array
    void PopulateTeam(UICT1Combatant[] team, Transform container)
    {
        for (int i = 0; i < CombatantsPerSide; i++)
        {
            GameObject f = Instantiate(EmptyCombatantPrefab, container);

            f.transform.position = container.position;
            f.transform.position += (transform.right * ((Screen.width/3) / CombatantsPerSide) * i);

            UICT1Combatant cbt = f.GetComponent<UICT1Combatant>();

            team[i] = cbt;

            UIselectable s = f.GetComponent<UIselectable>();

            s.OnExec += () =>
            {
                CancelTarget = s;
                Select(SpawnActionMenu(cbt));
            };

            if (i > 0)
            {
                s.NextLeft = team[i - 1].GetComponent<UIselectable>();
                team[i - 1].GetComponent<UIselectable>().NextRight = s;
            }

            f.SetActive(false); // deactivate
        }
    }

    // ui movement
    // 0=up, 1=right, 2=down, 3=left
    void MoveSelect(int dir)
    {
        UIselectable n = Selected.Next(dir);

        if (n != null)
        {
            Select(n);
            UIAudio.PlayOneShot(Snd_PointerMove, .5f);
        }

    }
    
    

    // this is called when you Select and Use a combatant
    // it spawns and populates an action menu
    UIselectable SpawnActionMenu(UICT1Combatant c)
    {
        GameObject g = Instantiate(ActionMenuPrefab, transform);

        g.GetComponentInChildren<Text>().text = c.CharacterName;
        
        UIselectable first = null;
        UIselectable prev = null;

        int total = 0;
        
        for (int i=0; i<c.ApplicableActions.Count; i++)
        {
            CombatAction ca = c.ApplicableActions[i];
            
            total++;

            UIselectable s;
            Text sl = null;

            // Instantiating buttons by custom prefab.
            // TODO: make 1 button prefab that gets populated from somewhere else
            if (ca.Name == "Attack")
            {
                s = Instantiate(ButtonPrefabAttack, g.transform).GetComponent<UIselectable>();
            }
            else if (ca.Name == "Use...")
            {
                s = Instantiate(ButtonPrefabUse, g.transform).GetComponent<UIselectable>();
            }
            else if (ca.Name == "Lewd")
            {
                s = Instantiate(ButtonPrefabLewd, g.transform).GetComponent<UIselectable>();
            }
            else
            {
                s = Instantiate(ButtonPrefabLewd, g.transform).GetComponent<UIselectable>();
            }

            // when we pass over the button, create a label
            s.OnSelect += () =>
            {
                sl = Instantiate(LabelPrefab, s.transform).GetComponent<Text>();
                sl.text = ca.Name;
                sl.transform.position = s.transform.position;
            };

            // if we cancel on any of buttons, destroy the menu
            s.OnCancel += () => { Destroy(g); };

            // if we move away from this button, destroy the text
            s.OnDeselect += () => { if (sl != null) Destroy(sl); };

            // when we Use the button, print some shit
            s.OnExec += () => 
            {
                print(CurTurn.CharacterName + " " + ca.Name + " " + c.CharacterName);

                Text t = Instantiate(CombatLogTextPrefab, CombatLogText).GetComponent<Text>();
                t.text = CurTurn.CharacterName + " " + ca.Name + " " + c.CharacterName;
                foreach (Transform tr in CombatLogs)
                {
                    tr.position += transform.up * 20;
                }
                CombatLogs.Add(t.transform);

                if (ca.Name == "Attack" && c.HurtScene != null && CurTurn.AttackScene != null)
                {
                    CombatViewL1.gameObject.SetActive(true);
                    CombatViewL2.gameObject.SetActive(true);

                    CombatViewL1.sprite = c.HurtScene;
                    CombatViewL2.sprite = CurTurn.AttackScene;

                    Vector3 cv1p = CombatViewL1.transform.position;
                    Vector3 cv2p = CombatViewL2.transform.position;

                    CombatViewL1.transform.position += (-transform.right * 500);
                    CombatViewL2.transform.position += (transform.right * 500);

                    CombatViewL1.color = new Color(1, 1, 1, 0);
                    CombatViewL2.color = new Color(1, 1, 1, 0);


                    CombatViewL1.transform.DOMove(cv1p, .5f);
                    CombatViewL2.transform.DOMove(cv2p, .5f);
                    CombatViewL1.DOColor(Color.white, .25f);
                    CombatViewL2.DOColor(Color.white, .25f);
                }
                else
                {
                    CombatViewL1.gameObject.SetActive(false);
                    CombatViewL2.gameObject.SetActive(false);
                }

                Cancel(); // back out of the menu
            };

            if (i == 0)
            {
                first = s;
            }
            else
            {
                if (prev != null)
                {
                    prev.NextDown = s;
                    s.NextUp = prev;
                }
            }

            prev = s;
        }

        g.transform.position = c.transform.position + (transform.up * 35 * (total+1));

        first.NextUp = prev;
        prev.NextDown = first; // loops!
        
        return first;
    }

    // when we select a new Selectable
    void Select(UIselectable s)
    {
        Pointer.transform.SetAsLastSibling(); // pointer always on top, just in case

        if (Selected != null) // if we have something selected before, trigger its deselection
            Selected.Deselect();
        
        Selected = s;
        Selected.Select();

    }
	
    // I'm just handling input stuff and simple smoothing for the Pointer in here
	void Update ()
    {
        // if the pointer isn't basically on top of its target, time-lerp there
        if (Vector3.Distance(Pointer.transform.position, Selected.transform.position) > .1f)
            Pointer.transform.position = Vector3.Lerp(Pointer.transform.position, Selected.transform.position + (Vector3)Selected.PointerOffset, 1-(Mathf.Pow(.1f, Time.deltaTime*20)));
        else // after we're sufficiently close, snap it there
            Pointer.transform.position = Selected.transform.position + (Vector3)Selected.PointerOffset;
        

        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            MoveSelect(0);
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
            MoveSelect(1);
        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
            MoveSelect(2);
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
            MoveSelect(3);

        if (Input.GetKeyDown(KeyCode.Space) && Selected != null && Selected.gameObject.activeInHierarchy)
        {
            Selected.Exec();
            UIAudio.PlayOneShot(Snd_Confirm, 0.7f);
        }

        // this only allows for 2 layers of menu, add more later.
        if (Input.GetKeyDown(KeyCode.Escape))
            Cancel();
    }

    void Cancel()
    {
        if (CancelTarget != null)
        {
            Selected.Cancel();

            Select(CancelTarget);

            CancelTarget = null;
        }
    }
}
