﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIselectable : MonoBehaviour {

    public Vector2 PointerOffset = Vector2.zero;

    public UIselectable NextUp;
    public UIselectable NextRight;
    public UIselectable NextDown;
    public UIselectable NextLeft;

    public delegate void UISelectableEvent();

    public UISelectableEvent OnExec;
    public UISelectableEvent OnSelect;
    public UISelectableEvent OnDeselect;
    public UISelectableEvent OnCancel;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public UIselectable Next(int dir)
    {
        //bahaha fuckin nested ternaries
        switch (dir)
        {
            case 0:
                return NextUp == null ? null : (NextUp.gameObject.activeInHierarchy ? NextUp : NextUp.Next(dir));
            case 1:
                return NextRight == null ? null : (NextRight.gameObject.activeInHierarchy ? NextRight : NextRight.Next(dir));
            case 2:
                return NextDown == null ? null : (NextDown.gameObject.activeInHierarchy ? NextDown : NextDown.Next(dir));
            case 3:
                return NextLeft == null ? null : (NextLeft.gameObject.activeInHierarchy ? NextLeft : NextLeft.Next(dir));
            default:
                return null;
        }
    }

    public void Exec()
    {
        if (OnExec != null)
            OnExec();
    }

    public void Select()
    {
        if (OnSelect != null)
            OnSelect();
    }
    public void Deselect()
    {
        if (OnDeselect != null)
            OnDeselect();
    }

    public void Cancel()
    {
        if (OnCancel != null)
            OnCancel();
    }
}
