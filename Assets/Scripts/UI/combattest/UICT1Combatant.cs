﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICT1Combatant : MonoBehaviour {

    public string CharacterName;

    public Sprite spr;
    public Sprite HurtScene;
    public Sprite AttackScene;
    
    // Holds our UI Image component
    Image img;

    // List of CombatActions that can be done to this character.
    public List<CombatAction> ApplicableActions = new List<CombatAction>();

    // Use this for initialization
    void Start ()
    {
        img = GetComponent<Image>();

        if (spr != null)
            img.sprite = spr;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

public class CombatAction
{
    public string Name;
    public Sprite Icon;

    public CombatAction(string name)
    {
        Name = name;
    }
}