﻿using NotifySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIArousal : Member {

    Text t;

    public override void OnNotify(string msg, object obj)
    {
        switch (msg){
            case "Player_AR":
                UpdateArousal((int)obj);
                break;
        }
    }


    // Use this for initialization
    void Start () {
        t = GetComponent<Text>();
        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().AddObserver(this);

	}
	
    void UpdateArousal(int value)
    {
        t.text = value.ToString();
    }
}
