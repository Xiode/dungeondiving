﻿using NotifySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIHealth : Member {

    Text t;

    public override void OnNotify(string msg, object obj)
    {
        switch (msg){
            case "Player_HP":
                UpdateHP((int)obj);
                break;
        }
    }


    // Use this for initialization
    void Start () {
        t = GetComponent<Text>();
        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().AddObserver(this);

	}
	
    void UpdateHP(int value)
    {
        t.text = value.ToString();
    }
}
