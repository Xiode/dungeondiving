﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEncounterDisplay : MonoBehaviour {

    Image img;
    
	// Use this for initialization
	void Start () {
        img = GetComponent<Image>();
	}

    public void UpdateDisplay(Sprite s)
    {
        img.sprite = s; // TODO: add effects, layers, etc
    }
	
}
