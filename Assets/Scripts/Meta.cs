﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Meta {

    public static int Current { get; internal set; }

    public delegate void MetaDelegate();

    public static MetaDelegate onTick;


    public enum _gameplayState
    {
        Unknown,
        Menu,
        Explore,
        Combat,
        Sex,
        Talk
    }

    public static _gameplayState GameplayState;

    // tick the game forward
    public static void Tick()
    {
        onTick();
        Current += 1;
    }
}
