﻿using NotifySystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EncounterSpawner : Member {

    public GameRules gameRules;

    TrapTrigger tt;

    public Encounter TheEncounter;

    // TODO: Choose possible enemies

    [Tooltip("Derives spawn frequency - This number times the total tile spaces the room has = how many turns it takes to spawn an encounter.")]
    public float SpawnFrequency;

    int TickCounter; // counts turns while Active
    int Ticks;
    int units; // total tile spaces the trigger volume encloses
    bool Active; // are we tracking turns and ready to spawn an encounter?

	// Use this for initialization
	void Start () {
        if (gameRules == null)
        {
            gameRules = GameDefaults.defaultGameRules;
        }

        AddObserver(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>());

        tt = GetComponent<TrapTrigger>();

        Vector3 m = GetComponent<MeshRenderer>().bounds.size;

        units = Mathf.FloorToInt((m.x / gameRules.StepSize) * (m.z / gameRules.StepSize));

        tt.OnEnter += (Collider c) =>
        {
            if (c.tag == "Player")
            {
                // Player has entered the encounter volume
                Ticks = Mathf.FloorToInt(units * SpawnFrequency);
                TickCounter = Ticks;
                Active = true;
            }
        };

        tt.OnExit += (Collider c) =>
        {
            if (c.tag == "Player")
            {
                // Player has left the encounter volume
                Active = false;
                print("whew!");
            }
        };

        Meta.onTick += () =>
        {
            if (Active)
            {
                TickCounter--;
                if (TickCounter <= 0)
                {
                    // Encounter!
                    TickCounter = Ticks;
                    Notify("StartEncounter", TheEncounter); // TODO: pick more than one encounter
                }
            }
        };

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void OnNotify(string msg, object obj)
    {
    }
}
