﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// made for testing the Turns system

public class ClockBlock : MonoBehaviour {

    Mover mv;

    bool bing;

	// Use this for initialization
	void Start () {
        mv = GetComponent<Mover>();

        Meta.onTick += () =>
        {
            mv.Move(bing ? transform.forward : -transform.forward);
            bing = !bing;
        };
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
