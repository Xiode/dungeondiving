﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDefaultsDefiner : MonoBehaviour {

    public GameRules defaultGameRules;

	// Use this for initialization
	void Start () {
        GameDefaults.defaultGameRules = defaultGameRules;
	}
}
