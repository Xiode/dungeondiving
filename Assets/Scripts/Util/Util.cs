﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util {
    
    // Source
    // https://www.scirra.com/blog/ashley/17/using-lerp-with-delta-time

    /// <summary>
    /// Perfect Lerp with Delta Time. Shorthand for Lerp(a, b, 1 - Mathf.Pow(f, deltatime))
    /// </summary>
    /// <param name="a">A</param>
    /// <param name="b">B</param>
    /// <param name="f">Factor</param>
    /// <param name="delta">Delta Time</param>
    /// <returns></returns>
    public static float pLerp(float a, float b, float f, float dt)
    {
        return Mathf.Lerp(a, b, 1 - Mathf.Pow(Mathf.Clamp(f, 0, 1), dt));
    }
}

public class Vector3Int
{
    public int x;
    public int y;
    public int z;
    
    public Vector3Int()
    {
        Init(0, 0, 0);
    }

    public Vector3Int(int X, int Y, int Z)
    {
        Init(X,Y,Z);
    }

    private void Init(int X, int Y, int Z)
    {
        x = X;
        y = Y;
        z = Z;
    }

    public static Vector3Int operator +(Vector3Int a, Vector3Int b)
    {
        return new Vector3Int(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    public static Vector3Int operator +(Vector3Int a, Vector3 b)
    {
        return new Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    public static Vector3Int operator *(Vector3Int a, int b)
    {
        return new Vector3Int(a.x*b, a.y*b, a.z*b);
    }

    public static implicit operator Vector3(Vector3Int a)
    {
        return new Vector3(a.x,a.y,a.z);
    }

    public static implicit operator Vector3Int(Vector3 v)
    {
        return new Vector3Int((int)v.x, (int)v.y, (int)v.z);
    }
}
