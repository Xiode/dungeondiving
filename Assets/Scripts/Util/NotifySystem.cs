﻿/* NotifySystem.cs
 * By: Xiode / Thomas Hall  2017
 * Implementation of the Observer pattern
 */

using System.Collections.Generic;
using UnityEngine;

namespace NotifySystem
{
    public static class NotifySystem
    {
        public static List<Member> SuperObservers = new List<Member>();
    }



    //template for a Member, that can act as both an observer and a subject.
    public abstract class Member : MonoBehaviour {

        protected List<Member> observers = new List<Member>();
        
        public void Notify(string msg)
        {
            Notify(msg, null, true);
        }

        public void Notify(string msg, object obj)
        {
            Notify(msg, obj, true);
        }

        public void Notify(string msg, object obj, bool registerFact)
        {
            Debug.Log("notify:" + msg + "//" + obj);
            foreach (Member o in observers)
            {
                o.OnNotify(msg, obj);
            }

            foreach (Member so in NotifySystem.SuperObservers)
            {
                so.OnNotify(msg,obj);
            }

            if (registerFact)
            {
                // fact
            }
            
        }
        /// <summary>
        /// Add an observer to this member. The observer will recieve all of this member's notifications.
        /// </summary>
        /// <param name="observer">The observer, who will recieve notifications.</param>
        public void AddObserver(Member observer)
        {
            if (observer != null)
                observers.Add(observer);
            else
                Debug.LogError("ERROR: Attempting to add null observer to object " + this.name);
     }

        /// <summary>
        /// Remove an observer from this member.
        /// </summary>
        /// <param name="observer">The observer, who will no longer recieve notifications from this member.</param>
        public void RemoveObserver(Member observer)
        {
            observers.Remove(observer);
        }
        
        public abstract void OnNotify(string msg, object obj);
    }
}