﻿/* Interactable.cs
 * By: Xiode / Thomas Hall  2017
 */

using NotifySystem;

public abstract class Interactable : Member {

    abstract public void Exec(); // this gets run when the player 'Uses' this interactable

    //all interactables are Members of the Notify system
    public override abstract void OnNotify(string msg, object obj);

}
