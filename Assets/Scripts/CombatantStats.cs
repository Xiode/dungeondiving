﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Combatant Stats")]
public class CombatantStats : ScriptableObject {

    public string Name;
    public bool Enemy;

    public int HP; // health
    public int MaxHP;
    public int AR; // arousal
    public int MaxAR;

    public Sprite IdleSprite;

    public CombatantAttack[] Attacks;
    public CombatantDefense[] Defenses;
    public CombatantLewd[] Lewds;
}
