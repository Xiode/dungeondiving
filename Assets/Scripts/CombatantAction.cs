﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatantAction : ScriptableObject {

    public string Name = "MyAction";
    public Sprite Frame;
}
