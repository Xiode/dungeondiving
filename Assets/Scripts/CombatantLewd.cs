﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Action - Lewd")]
public class CombatantLewd : CombatantAction {
    public int SelfArousal;
    public int TargetArousal;
}
