﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this is a test class to get the combat system working generally

public class CombatTest {

    public CTCombatant[] fighters;

    CTCombatant PlayerCombatant
    {
        get
        {
            return fighters[0];
        }
    }

    int CurTurn;
    bool WaitForInput = true;

	// Use this for initialization
	public CombatTest () {
        /*
        GInput.Bind(KeyCode.H, GInput.GVerb.menuSelect1);
        GInput.Bind(KeyCode.J, GInput.GVerb.menuSelect2);

        GInput.OnDown += (GInput.GVerb verb) =>
        {

            if (verb == GInput.GVerb.menuConfirm)
            {
                StartEncounter();
            }

            if (WaitForInput)
            {
                if (verb == GInput.GVerb.menuSelect1)
                {
                    if (CurTurn < 4)
                    {
                        
                        Debug.Log(fighters[CurTurn].Name + " attacks " + fighters[4].Name + " with " + fighters[CurTurn].Weapon.Name + ", dealing " + Attack(fighters[CurTurn], fighters[4]) + " damage!" );
                        Seek();
                    }
                    else if (CurTurn >= 4)
                    {

                        Debug.Log(fighters[CurTurn].Name + " attacks " + fighters[0].Name + " with " + fighters[CurTurn].Weapon.Name + ", dealing " + Attack(fighters[CurTurn], fighters[0]) + " damage!");
                        Seek();
                    }
                }
                else if (verb == GInput.GVerb.menuSelect2)
                {
                    if (CurTurn < 4)
                    {

                        Debug.Log(fighters[CurTurn].Name + " attacks " + fighters[5].Name + " with " + fighters[CurTurn].Weapon.Name + ", dealing " + Attack(fighters[CurTurn], fighters[5]) + " damage!");
                        Seek();
                    }
                    else if (CurTurn >= 4)
                    {
                        Debug.Log(fighters[CurTurn].Name + " attacks " + fighters[1].Name + " with " + fighters[CurTurn].Weapon.Name + ", dealing " + Attack(fighters[CurTurn], fighters[1]) + " damage!");
                        Seek();
                    }
                }
            }

        };*/
	}
	
    public void StartEncounter(Player pl, CombatantStats plstats, CombatantStats pa, Encounter e)
    {
        Debug.Log("starting encounter!");

        fighters = new CTCombatant[10];
        CurTurn = 0;

        CTWeapon Sword = new CTWeapon("Sword", 3);
        CTWeapon Bludgeon = new CTWeapon("Bludgeon", 2);
        CTWeapon Stick = new CTWeapon("Stick", 1);
        CTWeapon Hoof = new CTWeapon("Hoof", 0);

        CTArmor LeatherArmor = new CTArmor("Leather Armor", 1);
        CTArmor NoArmor = new CTArmor("No Armor", 0);


        for (int i=0; i<fighters.Length; i++)
        {
            fighters[i] = new CTCombatant(i); // make blank fighters
        }

        // Build the player's avatar and fill with stats
        fighters[0] = new CTCombatant(plstats, 0);

        fighters[0].Spr = pl.UIStatus.sprite;
        fighters[0].MaxHealth = pl.MaxHealth;
        fighters[0].Health = pl.Health;
        fighters[0].MaxArousal = pl.MaxArousal;
        fighters[0].Arousal = pl.Arousal;

        for (int i=0; i<e.enemies.Length; i++)
        {
            fighters[i+5] = new CTCombatant(e.enemies[i], i+5);
        }

        /*
        fighters[0] = new CTCombatant("Player", 0, 20, 0);
        fighters[0].EquipWeapon(Sword);
        fighters[0].EquipArmor(LeatherArmor);

        fighters[1] = new CTCombatant("Partner", 1, 15, 0);
        fighters[1].EquipWeapon(Bludgeon);
        fighters[1].EquipArmor(NoArmor);

        fighters[4] = new CTCombatant("Kobold", 4, 5, 0);
        fighters[4].EquipWeapon(Stick);
        fighters[4].EquipArmor(NoArmor);
        
        fighters[5] = new CTCombatant("Goat", 5, 3000, 0);
        fighters[5].EquipWeapon(Hoof);
        fighters[5].EquipArmor(NoArmor);
        */
    }

    // Seek to the next turn.
    public int Seek()
    {
        CurTurn++;

        if (CurTurn >= 10)
            CurTurn = 0;

        if (fighters[CurTurn] == null || fighters[CurTurn].Inactive)
        {
            return Seek(); // deepah
        }
        else
        {
            //Debug.Log(fighters[CurTurn].Name + "'s turn!");
            return CurTurn;
        }

    }

    int Attack(CTCombatant attacker, CTCombatant target)
    {
        return target.Hurt(attacker.Weapon.Dmg);
    }
    
}

public class CTCombatant
{
    public bool Inactive = true;

    public string Name;

    public int Health;
    public int Arousal;
    public int MaxHealth;
    public int MaxArousal;

    public CTWeapon Weapon;
    public CTArmor Armor;

    public int Index;

    public Sprite Spr;

    public bool IsEnemy
    {
        get
        {
            return Index > 4;
        }
    }
    
    public CTCombatant(int id)
    {
        Inactive = true;

        Index = id;

        Name = "N/A";
    }

    public CTCombatant(CombatantStats cs, int id)
    {
        Inactive = false;

        Name = cs.Name;

        Index = id;

        MaxHealth = cs.MaxHP;
        Health = cs.HP;

        MaxArousal = cs.MaxAR;
        Arousal = cs.AR;

        Spr = cs.IdleSprite;
    }

    public CTCombatant(string name, int id, int Hp, int Ar)
    {
        Inactive = false;

        Name = name;

        Index = id;

        MaxHealth = Hp;
        Health = Hp;

        MaxArousal = Ar;
        Arousal = Ar;
    }

    public void EquipWeapon(CTWeapon w)
    {
        Weapon = w;
    }

    public void EquipArmor(CTArmor a)
    {
        Armor = a;
    }
    
    // returns the dealt amount of damage
    public int Hurt(int Damage)
    {
        int dealt = Mathf.Max(Damage - Armor.Protection, 0);
        Health -= dealt;

        if (Health <= 0)
        {
            Debug.Log(Name + " died!");
            Inactive = true;
        }

        return dealt;
    }
}

public class CTWeapon
{
    public int Dmg;
    public string Name;

    public CTWeapon(string name, int dmg)
    {
        Dmg = dmg;
        Name = name;
    }
}

public class CTArmor
{
    public int Protection;
    public string Name;

    public CTArmor(string name, int protection)
    {
        Name = name;
        Protection = protection;
    }
}