﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/GameRules")]
public class GameRules : ScriptableObject {
    public float MovementSpeed = 20f;
    public int StepSize = 5;
}
